import ballerina/grpc;
import ballerina/io;

problem_1Client ep = check new ("http://localhost:9090");

public string loggedID = "";
public function main() {LoginUI ();
}

function readResponse(Create_courseStreamingClient create_courseStreamingClient) returns error? {
    string? result = check create_courseStreamingClient->receiveString();
    while !(result is ()) {
        io:println("Created course Code Received:", result);
        result = check create_courseStreamingClient->receiveString();
    }
}
function LoginUI (){
    io:println("            "+"LOGIN PAGE"+"            ");
    io:println("-----------------------------------------");
    string userID = io:readln("Please enter your user userID: ");
    string password = io:readln("Please enter your password: ");

    LoginResponse|grpc:Error? res = ep->login({userID: userID, password: password});

    if (res is grpc:Error) {
        io:println("Please try again something went wrong");
    } else if (res["success"] === true) {
        loggedID = userID;
        io:println("You've successfully logged in");

        if (res["profile"] == ADMINISTRATOR) {
            AdminDashboardUI();
        } else if (res["profile"] == ASSESSOR) {
            AssessorDashboardUI();
        } else {
            StudentDashboardUI();
        }

    } else {
        io:println("Login failed! Invalid credentials.");
        LoginUI();
    }
}

function AdminDashboardUI() {
   io:println("            "+"ADMIN PAGE"+"            ");
    io:println("-----------------------------------------");
    io:println("1. Create a course");
    io:println("2. Assign assessor to course");
    io:println("3. Create user");
    io:println("4. Logout");
    string choice = io:readln("Enter choice 1-4: ");

    if (choice === "1") {
        CreateCourseUI();
    } else if (choice === "2") {
        AssignAssessorUI();
    } else if (choice === "3") {
        CreateUserUI();
    } else if (choice === "4") {
        loggedID = "";
        LoginUI();
    } else {
        io:println("Invalid input");
        AdminDashboardUI();
    }
}

function CreateCourseUI() {
    Assignment[] assignments = [];
    CourseRequest[] courses = [];
    int count = 0;

    while (true) {
        string name = io:readln("Course name: ");
        string courseCode = io:readln("Course code: ");
        while (true) {
            string weight = io:readln("Enter Assignment weight or '0' to proceed: ");
            float floatWeight = checkpanic float:fromString(weight);

            count += 100;

            assignments.push({"weight": floatWeight, "id": count});
            
            if (weight === "0") {
                break;
            }
        }

        courses.push({name: name, assignments: assignments, courseCode: courseCode});
        string choice = io:readln("Enter '1' to create another course or '0' to proceed: ");
        if (choice === "0") {
            break;
        }
    }

    Create_courseStreamingClient|grpc:Error streamingClient = ep->create_course();

    if (streamingClient is grpc:Error) {
        io:println("Error from Connector: " + streamingClient.message() + " - ");
        return;
    } else {
        future<error?> f1 = start readResponse(streamingClient);

        foreach CourseRequest val in courses {
            io:println("Sent course details: ", val);
            checkpanic streamingClient->sendCourseRequest(val);
        }

        checkpanic streamingClient->complete();
        io:println("Closed stream");

        checkpanic wait f1;
    }

    AdminDashboardUI();
}

function AssignAssessorUI() {
    string courseID = io:readln("Course ID: ");
    string assessorID = io:readln("Assessor ID: ");

    boolean|grpc:Error result = ep->assign_course({courseCode: courseID, assessorCode: assessorID});

    if (result is boolean) {
        if (result) {
            io:println("Assessor assigned");
        } else {
            io:println("Failed to assign Assessor");
        }
    }

    AdminDashboardUI();
}

function CreateUserUI() {
    string userCode = io:readln("UserID: ");
    string name = io:readln("Full name: ");
    io:println("What type of user are you creating?");
    io:println("1. Administrator");
    io:println("2. Assessor");
    io:println("3. Learner");
    string assessorID = io:readln("Select profile: ");

    Create_userStreamingClient|grpc:Error userStream = ep->create_user();

    if (userStream is grpc:Error) {
        io:println("Error from Connector: " + userStream.message() + " - ");
    } else {
        checkpanic userStream->sendUserRequest({userCode: userCode, name: name, profile: (assessorID === "1") ? ADMINISTRATOR : (assessorID === "2") ? ASSESSOR : LEARNER});
    }

    AdminDashboardUI();
}

function StudentDashboardUI() {
    io:println("            "+"LOGIN PAGE"+"            ");
    io:println("-----------------------------------------");
    io:println("1. Submit assignment");
    io:println("2. Register to course");
    io:println("3. Logout");

    string choice = io:readln("Enter choice 1-3: ");

    if (choice === "1") {
        SubmitAssignment();
    } else if (choice === "2") {
        CourseRegister();
    } else if(choice === "3") {
        loggedID = "";
        LoginUI();
    }else {
        io:println("Invalid input");
        StudentDashboardUI();
    }
}

function SubmitAssignment() {
    AssignmentRequest[] assignments = [];
    while true {
        string courseCode = io:readln("Course code: ");
        string assignmentID = io:readln("Assignment ID: ");
        string content = io:readln("Content: ");

        assignments.push({courseCode: courseCode, studentID: loggedID, assignmentID: checkpanic int:fromString(assignmentID), content: content});
        string choice = io:readln("Enter '1' to submit another assignment or '0' to proceed: ");
        if (choice == "0") {
            break;
        }
    }

    Submit_assignmentStreamingClient|grpc:Error streamingClient = ep->submit_assignment();

    if (streamingClient is grpc:Error) {
        io:println("Error from Connector: " + streamingClient.message() + " - ");
        return;
    } else {

        foreach AssignmentRequest val in assignments {
            checkpanic streamingClient->sendAssignmentRequest(val);
        }

        checkpanic streamingClient->complete();
        io:println("Closed stream");

        boolean|grpc:Error? response = streamingClient->receiveBoolean();

        if (response === true) {
            io:println("Assignments submitted");
        } else {
            io:println("Failed to submit Assignments");
        }

    }

    StudentDashboardUI();

}

function CourseRegister() {
    string[] courses = [];

    while (true) {
        string courseID = io:readln("Course ID: ");

        courses.push(courseID);

        string choice = io:readln("Enter '1' to register for another course or '0' to proceed: ");
        if (choice == "0") {
            break;
        }
    }

    RegisterStreamingClient|grpc:Error registerStream = ep->register();

    if (registerStream is grpc:Error) {
        io:println("Error from Connector: " + registerStream.message() + " - ");
        return;
    } else {
        foreach string courseID in courses {

            checkpanic registerStream->sendRegister({courseID: courseID, userID: loggedID});
        }

        checkpanic registerStream->complete();
        boolean|grpc:Error? result = registerStream->receiveBoolean();

        if (result is boolean) {
            if (result) {
                io:println("Registered for courses");
            } else {
                io:println("Failed to register for courses");

            }
        }
    }

    StudentDashboardUI();

}

function AssessorDashboardUI() {
    io:println("            "+"Assesor PAGE"+"            ");
    io:println("-----------------------------------------");
    io:println("1. Get unmarked assignments");
    io:println("2. Submit marks");
    io:println("3. Logout");
    string choice = io:readln("Enter choice 1-3: ");

    if (choice === "1") {
        GetAssignments();
    } else if (choice === "2") {
        SubmitMark();
    }else if (choice === "3") {
        loggedID = "";
        LoginUI();
    } else {
        io:println("Invalid input");
        AssessorDashboardUI();
    }

}

function GetAssignments() {

    stream<Submission, grpc:Error?>|grpc:Error streamResult = ep->request_assignment({courseCode: "dsp", assignmentID: 200, assessorCode: "assessor"});
    if (streamResult is grpc:Error) {
        io:println("Error from Connector: " + streamResult.message() + " - ");
        return;
    } else {
        checkpanic streamResult.forEach(function(Submission submission) {
            io:println(submission);
        });
    }

    AssessorDashboardUI();
}

function SubmitMark() {
    string mark = io:readln("Enter mark: ");
    string studentID = io:readln("Enter student ID: ");
    string courseID = io:readln("Enter course ID: ");
    string assignmentID = io:readln("Enter assignment ID: ");
    boolean|grpc:Error result = ep->submit_mark({
        mark: checkpanic float:fromString(mark),
        studentID: studentID,
        courseID: courseID,
        assignmentID: checkpanic int:fromString(assignmentID)
    });

    io:println(result);
    if (result is boolean) {
        if (result == true) {
            io:println("Marked assignment");
        } else {
            io:println("Failed to mark assignment");
        }
    }

    AssessorDashboardUI();
}