import ballerina/http;
import ballerina/io;

public function main() returns error? {
    // Creates a new client with the backend URL.
    final http:Client clientEndpoint = check new ("http://localhost:4000");

    while (true) {

        io:println("What would you like to do?");
        io:println("1. Create student");
        io:println("2. Update student details");
        io:println("3. Update student course details");
        io:println("4. Look up student");
        io:println("5. Get all students");
        io:println("6. Delete student");

        string choice = io:readln("Enter choice 1-6: ");

        if (choice == "1") {
            string id = io:readln("Enter student number: ");
            string name = io:readln("Enter name: ");
            string email = io:readln("Enter email: ");
            string address = io:readln("Enter address: ");

            json resp = check clientEndpoint->post("/create", {studentNumber: id, name: name, email: email, address: address});
            io:println(resp.toJsonString());

        } else if (choice == "2") {
            string studentNumber = io:readln("Enter student number: ");

            io:println("Enter new value or leave empty to skip.");
            string name = io:readln("Enter name: ");
            string email = io:readln("Enter email: ");
            string address = io:readln("Enter address: ");

            json resp = check clientEndpoint->put("/update", {studentNumber: studentNumber, name: name, email: email, address: address});
            io:println(resp.toJsonString());

        } else if (choice == "3") {
            string studentNumber = io:readln("Enter student number: ");
            string course = io:readln("Enter course code: ");

            json resp = check clientEndpoint->put("/course", {studentNumber: studentNumber, course: course});
            io:println(resp.toJsonString());

        } else if (choice == "4") {
            string studentNumber = io:readln("Enter student number: ");

            json resp = check clientEndpoint->get("/lookup?studentNumber=" + studentNumber);
            io:println(resp);
        } else if (choice == "5") {
            json resp = check clientEndpoint->get("/all");
            io:println(resp);
        } else if (choice == "6") {
            string studentNumber = io:readln("Enter student number: ");
            json resp = check clientEndpoint->delete("/delete?studentNumber=" + studentNumber);
            io:println(resp);
        }
    }
}
