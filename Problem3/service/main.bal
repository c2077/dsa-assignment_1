import ballerina/http;

import ballerina/io;

public type Student record {|
    readonly string studentNumber;
    string email;
    string name;
    string address;
    string[] courses = [];
|};

public type Submission record {|
    readonly string studentNumber;
    float mark;
    string content;
|};

public type Assignment record {|
    readonly string id;
    float weight;
    table<Submission> submissions;
|};

public type Course record {|
    readonly string code;
    table<Assignment> assignments;
|};

table<Student> key(studentNumber) students = table [
        {studentNumber: "21345432", name: "Beaver", email: "beaver@gmail.com", address: "", courses: ["BMC211"]}
            ];

table<Course> key(code) courses = table [
        {
            code: "BMC211",
            assignments: table [
                            {
                                id: "2114",
                                weight: 0.5,
                                submissions: table [
                                    {
                                        studentNumber: "21345432",
                                        content: "This is the submission",
                                        mark: 0.0
                                    }
                                ]
                            }
                        ]
        }
];

public type StudentRequest record {|
    string studentNumber;
    string email = "";
    string name = "";
    string address = "";
|};

public type CourseRequest record {|
    string studentNumber;
    string course;
|};

service / on new http:Listener(4000) {
    // path to create a new student account
    resource function post create(@http:Payload Student payload) returns json {
        // string? studentNumber = payload.studentNumber;
        students.add({studentNumber: payload.studentNumber, name: payload.name, email: payload.email, address: payload.address, courses: []});

        io:println(students.toBalString());
        return {success: true, message: "Student created successfully."};
    }

    resource function put update(@http:Payload StudentRequest payload) returns json {

        io:println(payload.toBalString());
        Student student = students.get(payload.studentNumber);

        if (payload.address !== "") {
            student.address = payload.address;

        }

        if (payload.email !== "") {
            student.email = payload.email;

        }

        if (payload.name !== "") {
            student.name = payload.name;
        }

        return {success: true, message: "Updated student details"};

    }

    resource function put course(@http:Payload CourseRequest payload) returns json {

        Student student = students.get(payload.studentNumber);
        student.courses.push(payload.course);

        return {success: true, message: "Added course"};
    }

    resource function get lookup(string studentNumber) returns Student {
        return students.get(studentNumber);
    }

    resource function get all() returns json {
        return students.toJson();
    }

    resource function delete delete(string studentNumber) returns json {
        Student result = students.remove(studentNumber);

        io:println(students.toBalString());
        return {success: true, message: "Student removed"};

    }
}
